import React, { Fragment, useState } from "react";
import { ICard, IList } from "types/common";
import { AddCard, AddListInput, Card, ListItem } from "../components";
import "../style/Trello.css";
import { getListFromLocalStorage } from "../utils";

const Trello = () => {
  const [createListId, setCreateListId] = useState<number>(0);
  const [list, setList] = useState<IList[]>(getListFromLocalStorage());

  return (
    <div className="main-container">
      <div className="grid-container">
        {list?.map((item: IList, index: number) => (
          <Fragment key={index}>
            <ListItem
              createListId={createListId}
              item={item}
              setCreateListId={setCreateListId}
              setList={setList}
            />
          </Fragment>
        ))}
        <div className="grid-item">
          <AddListInput setList={setList} />
        </div>
      </div>
    </div>
  );
};

export default Trello;
