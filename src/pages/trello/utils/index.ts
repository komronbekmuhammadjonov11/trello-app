import getListFromLocalStorage from "./getListFromLocalStorage/getListFromLocalStorage";
import saveToListLocalStorage from "./saveToListLocalStorage/saveToListLocalStorage";

export { getListFromLocalStorage, saveToListLocalStorage };
