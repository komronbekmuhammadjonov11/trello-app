import { IList } from "types/common";

function getListFromLocalStorage(): IList[] {
  const listStorage = localStorage.getItem("list") as string | undefined | null;
  const parseList: IList[] = listStorage ? JSON.parse(listStorage) : [];
  return parseList;
}

export default getListFromLocalStorage;
