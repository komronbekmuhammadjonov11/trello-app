import { IList } from "types/common";

function saveToListLocalStorage(list: Required<IList[]>): void {
  list && localStorage.setItem("list", JSON.stringify(list));
}

export default saveToListLocalStorage;
