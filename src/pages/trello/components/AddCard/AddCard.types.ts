import { Dispatch, SetStateAction } from "react";
import { IList } from "types/common";

export interface IAddCard {
  id: number;
  createListId: number;
  setCreateListId: Dispatch<SetStateAction<number>>;
  setList: Dispatch<SetStateAction<IList[]>>;
}
