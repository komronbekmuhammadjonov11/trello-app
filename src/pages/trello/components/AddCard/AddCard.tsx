import React, { FC, Fragment } from "react";
import CancelIcon from "assets/icons/CancelIcon";
import { Button, IconButton, TextArea } from "components";
import { IList } from "types/common";
import { IAddCard } from "./AddCard.types";
import "./AddCard.css";
import PlusIcon from "assets/icons/PlusIcon";
import { saveToListLocalStorage } from "pages/trello/utils";

const AddCard: FC<IAddCard> = ({
  id,
  createListId,
  setCreateListId,
  setList,
}) => {
  const listCardUpdate = (message: string): void => {
    if (message) {
      setList((prev: IList[]) => {
        const newList = prev?.map((list) => {
          if (list.id === id) {
            const { cardList } = list;
            return {
              ...list,
              cardList: cardList.concat({ id: Date.now(), message }),
            };
          }
          return list;
        });
        saveToListLocalStorage(newList);
        return newList;
      });
    }
  };

  const addCard = async (event: any): Promise<void> => {
    event.preventDefault();
    const formData = new FormData(event.target);
    const message = formData.get(id?.toString());
    if (typeof message === "string") {
      await listCardUpdate(message);
      await event.target.reset();
      setCreateListId(0);
    }
  };

  const clearCreateListId = (): void => {
    setCreateListId(0);
  };

  const addCardSectionClick = (): void => {
    setCreateListId(id);
  };

  return (
    <Fragment>
      {createListId === id ? (
        <form className="add-card-form" onSubmit={addCard}>
          <div className="add-card-textarea">
            <TextArea
              placeholder="Enter a title this card..."
              name={id.toString()}
              required
              autoFocus
            />
          </div>

          <div className="add-card-actions">
            <Button type="submit">Add list</Button>
            <IconButton className="cancel-button" onClick={clearCreateListId}>
              <CancelIcon width="11px" height="11px" />
            </IconButton>
          </div>
        </form>
      ) : (
        <div className="add-card-section" onClick={addCardSectionClick}>
          <PlusIcon width="14px" height="14px" fill="#333" />
          <span>Add a card</span>
        </div>
      )}
    </Fragment>
  );
};

export default AddCard;
