import { Dispatch, SetStateAction } from "react";
import { IList } from "types/common";

export interface IAddListForm {
  setList: Dispatch<SetStateAction<IList[]>>;
}

export type TviewState = "default" | "add";
