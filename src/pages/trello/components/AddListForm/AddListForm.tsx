import React, { FC, Fragment, useState } from "react";
import { IAddListForm, TviewState } from "./AddListForm.types";
import "./AddListForm.css";
import { Button, IconButton, Input } from "components";
import CancelIcon from "assets/icons/CancelIcon";
import PlusIcon from "assets/icons/PlusIcon";
import { IList } from "types/common";
import { saveToListLocalStorage } from "pages/trello/utils";

const AddListInput: FC<IAddListForm> = ({ setList }) => {
  const [viewState, setViewState] = useState<TviewState>("default");

  const addListForm = (event: any): void => {
    event.preventDefault();
    const formData = new FormData(event?.target);
    const title = formData.get("title");
    if (typeof title === "string") {
      setList((prev: IList[]) => {
        const newList = prev.concat({
          cardList: [],
          id: Date.now(),
          title: title.trim(),
        });
        saveToListLocalStorage(newList);
        return newList;
      });
      event.target.reset();
      cancelIconClick();
    }
  };

  const defaultCardClick = (): void => {
    setViewState("add");
  };

  const cancelIconClick = () => {
    setViewState("default");
  };

  return (
    <Fragment>
      {viewState === "add" && (
        <form onSubmit={addListForm} className="add-list-form">
          <div className="add-list-input">
            <Input
              placeholder="Enterlist title..."
              name="title"
              required
              autoFocus
            />
          </div>
          <div className="add-list-actions">
            <Button type="submit">Add list</Button>
            <IconButton className="cancel-button" onClick={cancelIconClick}>
              <CancelIcon width="11px" height="11px" />
            </IconButton>
          </div>
        </form>
      )}
      {viewState === "default" && (
        <div className="add-list-default-card" onClick={defaultCardClick}>
          <PlusIcon width="14px" height="14px" fill="#fff" />

          <span> Add another list</span>
        </div>
      )}
    </Fragment>
  );
};

export default AddListInput;
