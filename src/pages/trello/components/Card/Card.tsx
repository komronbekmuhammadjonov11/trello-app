import React, { FC, useState } from "react";
import { ICardProps } from "./Card.types";
import "./Card.css";
import EditIcon from "assets/icons/EditIcon";
import { IconButton } from "components";
import { ICard, IList } from "types/common";

const Card: FC<ICardProps> = ({ card, setList, item }) => {
  // states

  const [currentCard, setCurrentCard] = useState<ICard>();
  const [currentItem, setCurrentItem] = useState<IList>();

  const onDragStart = (
    event: React.DragEvent<HTMLDivElement>,
    dragStartCard: ICard,
    item: IList
  ): void => {
    setCurrentCard(dragStartCard);
    setCurrentItem(item);
    event.dataTransfer?.setData("text", event.currentTarget.id);
  };

  const onDragOver = (event: React.DragEvent<HTMLDivElement>): void => {
    event.preventDefault();
    const element = event.target as HTMLDivElement;
    if (element.className == "main-card") element.classList.add("drag-over");
  };

  const onDragLeave = (event: React.DragEvent<HTMLDivElement>): void => {
    const element = event.target as HTMLDivElement;
    element.classList.remove("drag-over");
  };

  const onDragEnd = (event: React.DragEvent<HTMLDivElement>): void => {
    const element = event.target as HTMLDivElement;
    element.classList.remove("drag-over");
  };

  console.log("🚀 ~ file: Card.tsx ~ line 12 ~ currentCard", currentCard);
  const onDrop = (
    event: React.DragEvent<HTMLDivElement>,
    dropCard: ICard,
    dropItem: IList
  ): void => {
    console.log("🚀 ~ file: Card.tsx ~ line 12 ~ currentCard", currentCard);
    event.preventDefault();
    if (currentCard && currentItem) {
      const element = event.target as HTMLDivElement;
      element.classList.remove("drag-over");
      const currentIndex = currentItem?.cardList.indexOf(currentCard);
      currentItem?.cardList?.splice(currentIndex, 1);
      const dropIndex = dropItem?.cardList?.indexOf(dropCard);
      dropItem?.cardList?.splice(dropIndex, 0, currentCard);
      setList((prev) => {
        const newList = prev.map((listItem) => {
          if (currentItem.id === listItem.id) return currentItem;
          if (dropItem.id === listItem.id) return dropItem;
          return listItem;
        });

        return newList;
      });
    }
  };

  return (
    <div
      className="main-card"
      draggable={true}
      onDragStart={(event) => onDragStart(event, card, item)}
      onDragEnd={(event) => onDragEnd(event)}
      onDragOver={(event) => onDragOver(event)}
      onDrop={(event) => onDrop(event, card, item)}
      onDragLeave={onDragLeave}
      id={card.id.toString()}
    >
      <IconButton className="main-card-edit-icon">
        <EditIcon width="12px" height="12px" />
      </IconButton>
      <p className="main-card-message">{card?.message}</p>
    </div>
  );
};

export default Card;
