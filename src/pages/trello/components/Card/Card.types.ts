import { Dispatch, SetStateAction } from "react";
import { ICard, IList } from "types/common";

export interface ICardProps {
  card: ICard;
  setList: Dispatch<SetStateAction<IList[]>>;
  item: IList;
}
