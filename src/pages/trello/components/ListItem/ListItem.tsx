import React, { FC, Fragment } from "react";
import { ICard } from "types/common";
import AddCard from "../AddCard/AddCard";
import Card from "../Card/Card";
import { IListItem } from "./ListItem.types";

const ListItem: FC<IListItem> = ({
  createListId,
  setCreateListId,
  item,
  setList,
}) => {
  return (
    <div className="grid-item">
      <div className="main-title">{item.title}</div>
      {item?.cardList?.map((card: ICard, index: number) => (
        <Fragment key={index}>
          <Card card={card} setList={setList} item={item} />
        </Fragment>
      ))}
      <AddCard
        id={item.id}
        createListId={createListId}
        setCreateListId={setCreateListId}
        setList={setList}
      />
    </div>
  );
};

export default ListItem;
