import { Dispatch, SetStateAction } from "react";
import { ICard, IList } from "types/common";

export interface IListItem {
  createListId: number;
  setCreateListId: Dispatch<SetStateAction<number>>;
  item: Required<IList>;
  setList: Dispatch<SetStateAction<Required<IList>[]>>;
}
