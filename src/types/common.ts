export interface IIcon {
  fill?: string;
  xmlns?: string;
  viewBox?: string;
  width?: string;
  height?: string;
}

export interface IList {
  id: number;
  title: string;
  cardList: ICard[];
}

export interface ICard {
  id: number;
  message: string;
}
