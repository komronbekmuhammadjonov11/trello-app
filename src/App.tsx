import React from "react";
import Trello from "pages/trello/container";

function App() {
  return (
    <div className="App">
      <Trello />
    </div>
  );
}

export default App;
