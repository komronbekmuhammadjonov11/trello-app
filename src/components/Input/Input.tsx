import React, { FC } from "react";
import { IInput } from "./Input.types";
import "./Input.css";

const Input: FC<IInput> = ({ ...props }) => {
  return (
    <label className="input-container">
      <input type="text" className="main-input" {...props} />
    </label>
  );
};

export default Input;
