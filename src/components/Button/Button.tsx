import React, { FC } from "react";
import { IButton } from "./Button.types";
import "./Button.css";
const Button: FC<IButton> = ({ children, ...props }) => {
  return (
    <button role="button" className="main-button" {...props}>
      <span className="text">{children}</span>
    </button>
  );
};

export default Button;
