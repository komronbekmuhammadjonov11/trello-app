import React from "react";

export interface ITextArea {
  name?: string;
  placeholder?: string;
  required?: boolean;
  className?: string;
  autoFocus?: boolean;
  onChange?: (value: React.ChangeEvent<HTMLTextAreaElement>) => void;
}
