import React, { FC } from "react";
import { ITextArea } from "./TextArea.types";
import "./TextArea.css";
const TextArea: FC<ITextArea> = ({ className, ...props }) => {
  return <textarea className={`main-text-area ${className}`} {...props} />;
};

export default TextArea;
