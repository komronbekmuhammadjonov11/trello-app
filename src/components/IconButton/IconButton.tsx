import React, { FC } from "react";
import { IIconButton } from "./IconButton.types";
import "./IconButton.css";
const IconButton: FC<IIconButton> = ({ children, className, ...props }) => {
  return (
    <button type="button" className={`icon-button ${className}`} {...props}>
      {children}
    </button>
  );
};

export default IconButton;
