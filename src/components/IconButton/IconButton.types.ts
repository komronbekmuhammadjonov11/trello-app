import React, { ReactNode } from "react";

export interface IIconButton {
  children: ReactNode;
  className?: string;
  onClick?: () => void;
}
